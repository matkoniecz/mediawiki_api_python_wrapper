rm dist -rf
python3 setup.py sdist bdist_wheel
cd dist
pip3 uninstall mediawiki_api_wrapper -y
pip3 install --user *.whl
python3.10 -m pip uninstall mediawiki_api_wrapper -y
python3.10 -m pip install --user *.whl
cd ..
python3 -m unittest
# twine upload dist/* # to upload to PyPi
