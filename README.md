# MediaWiki API rapper

It is an unofficial Python wrapper over available API.

It is a Python wrapper for a tiny part of mediawiki API.

It was written to interact with OSM Wiki and some things may be assuming it too much - but PRs changing this are welcome.

Published as a Python package, see [https://pypi.org/project/mediawiki-api-wrapper/](https://pypi.org/project/mediawiki-api-wrapper/)


# Examples
<!-- in case of editing or adding samples here, change also tests -->


```
import mediawiki_api_wrapper

???TODO (please open an issue if you planned to use that repository)
```

# Development

Contributions are welcome to cover larger part of API.

## Run tests

`python3 -m unittest`