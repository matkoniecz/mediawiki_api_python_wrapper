import unittest
import mediawiki_api_wrapper

class Tests(unittest.TestCase):
    def test_basic_math(self):
        self.assertEqual(2-2, 0)

if __name__ == '__main__':
    unittest.main()
